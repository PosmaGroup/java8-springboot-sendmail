package com.posma.mail.app.controllers.dto;

/**
 * Created by Oswaldo Zarate on 8/5/2016.
 */
public class ContactDto {

    public String name;
    public String email;
    public String description;

    public ContactDto() {
    }

    public ContactDto(String name, String email, String description) {
        this.name = name;
        this.email = email;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
