package com.posma.mail.app.controllers;

import com.posma.mail.app.business.ContactService;
import com.posma.mail.app.controllers.dto.ContactDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by Oswaldo Zarate on 8/5/2016.
 */

@Controller
@RequestMapping("posma/api/contact")
public class ContactController {

    @Autowired
    private ContactService contactService;

    @RequestMapping(method = RequestMethod.GET)
    @ResponseBody
    public String get() {
        return "Test service";
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<String> post(@RequestBody ContactDto request){
        try{
            if(contactService.SendContactRequest(request)){
                return new ResponseEntity<String>("Exitoso", HttpStatus.OK);
            }
            else{
                return new ResponseEntity<String>("Fallo",HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
        catch(Exception ex){
            return new ResponseEntity<String>(ex.toString(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
