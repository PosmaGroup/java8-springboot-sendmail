package com.posma.mail.app.business;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.UnsupportedEncodingException;
import java.util.Properties;
import com.posma.mail.app.controllers.dto.ContactDto;

@Service
public class ContactService {

    Properties mailServerProperties;
    Session getMailSession;
    MimeMessage generateMailMessage;
    @Autowired
    SmtpAuthenticator smtpAuthenticator;

    public boolean SendContactRequest(ContactDto request) throws MessagingException, UnsupportedEncodingException {
        try {
            // Step1
            System.out.println("\n 1st ===> setup Mail Server Properties..");
            mailServerProperties = System.getProperties();
            mailServerProperties.put("mail.smtp.port", "587");
            mailServerProperties.put("mail.smtp.auth", "true");
            mailServerProperties.put("mail.smtp.starttls.enable", "true");
            mailServerProperties.put("mail.smtp.host", "smtp.gmail.com");

            System.out.println("Mail Server Properties have been setup successfully..");

            // Step2
            System.out.println("\n\n 2nd ===> get Mail Session..");
            //Create a Session object & authenticate uid and pwd
            getMailSession = Session.getInstance(mailServerProperties, new javax.mail.Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return smtpAuthenticator.getPasswordAuthentication();
                }
            });

            generateMailMessage = new MimeMessage(getMailSession);
            generateMailMessage.addRecipient(Message.RecipientType.TO, new InternetAddress("posmagroup@gmail.com", "Posma Group Informacion")); //Repit if need more recipient to send
            generateMailMessage.setSubject("Potencial Contacto");
            String emailBody = "Nombre: " + request.getName() + "<br>Email: " + request.getEmail() + "<br><br><br>Mensaje: " + request.getDescription();
            generateMailMessage.setContent(emailBody, "text/html");
            System.out.println("Mail Session has been created successfully..");

            // Step3
            System.out.println("\n\n 3rd ===> Get Session and Send mail");
            Transport.send(generateMailMessage);

            return true;
        }catch (Exception ex){
            return false;
        }
    }
}
